version: 2.1

alias_anchors: # anchors must be defined in YAML before being used by an alias
  - &should_sign
    or:
      - equal: [ develop, << pipeline.git.branch >> ]
      - equal: [ main, << pipeline.git.branch >> ]
      - equal: [ master, << pipeline.git.branch >> ]
      - equal: [ "ng-csharp-xamarin", << pipeline.git.branch >> ]
workflows:
  version: 2
  build:
    jobs:
      - build-mac:
          context: scratch-desktop-and-link
jobs:
  build-mac:
    macos:
      xcode: 13.4
    steps:
      - checkout
      - run:
          name: Build Safari helper
          command: |
            xcodebuild -project "Scratch Link Safari Helper/Scratch Link Safari Helper.xcodeproj" -scheme "Scratch Link Safari Helper" clean build
      - restore_cache:
          keys:
            - visual-studio-mac-v3
      - run:
          name: Install/Extract Visual Studio for Mac
          command: |
            # CACHE_ARCHIVE must match the path in save_cache
            CACHE_ARCHIVE="/tmp/visual-studio-mac-v3.tar.gz"
            bash ./.circleci/install-or-extract-vs-mac.sh "$CACHE_ARCHIVE"
      - save_cache:
          key: visual-studio-mac-v3
          paths: # Caching an archive instead of loose files works around CircleCI's issues with extracting links and permissions on macOS
            - /tmp/visual-studio-mac-v3.tar.gz
      - unless:
          condition:
            *should_sign
          steps:
            - do_mac_build:
                configuration: Debug
                artifact_tag: Debug
      - when:
          condition:
            *should_sign
          steps:
            - run:
                name: Set up code-signing keychain # Keep this after long install steps to avoid keychain timeout
                # DO NOT RUN THIS on your own computer unless you really know what you're doing!
                command: |
                  function decodeToFile () {
                    if [ -z "$1" ]; then
                      echo "Missing or invalid filename"
                      return 1
                    fi
                    if [ -z "$2" ]; then
                      echo "Missing environment variable contents for file: $1"
                      return 2
                    fi
                    echo "$2" | base64 --decode > "$1"
                  }
                  decodeToFile macos-certs-scratch-foundation.p12 "${CSC_MACOS}"
                  security -v create-keychain -p circleci circleci.keychain
                  security -v default-keychain -s circleci.keychain
                  security -v import macos-certs-scratch-foundation.p12 -k circleci.keychain -P "${CSC_MACOS_PASSWORD}" -T /usr/bin/codesign -T /usr/bin/productbuild
                  security -v unlock-keychain -p circleci circleci.keychain
                  # "set-key-partition-list" prints extensive not-so-useful output and adding "-q" (even multiple times) doesn't suppress it.
                  # The "grep -v" at the end of this line suppresses all of that so any errors or warnings might be more visible.
                  security -v set-key-partition-list -S apple-tool:,apple:,codesign: -s -k circleci circleci.keychain | grep -v '^    0x'
                  security -v set-keychain-settings -lut 600 circleci.keychain
                  security -v find-identity circleci.keychain
                  rm macos-certs-scratch-foundation.p12
            - run:
                name: Build and sign the Safari extension
                command: |
                  # clean both, then build both, in case their "clean" steps overlap
                  for TASK in clean build; do
                    for CONFIG in Release_DevID Release_MAS; do
                      xcodebuild -configuration $CONFIG -project "Scratch Link Safari Helper/Scratch Link Safari Helper.xcodeproj" -target "Scratch Link Safari Extension" -destination "name=Any Mac" $TASK
                    done
                  done
            - do_mac_build:
                configuration: Release_DevID
                artifact_tag: notarizeMe # see below
            - do_mac_build:
                configuration: Release_MAS
                artifact_tag: MAS
            - run:
                name: Notarize Developer ID build
                command: |
                  for PKGPATH in Artifacts/"Scratch Link-notarizeMe"*.pkg; do
                    PKGFILE="${PKGPATH##*/}"
                    scratch-link-mac/notarize.sh edu.mit.scratch.scratch-link "${PKGPATH}" "${PKGPATH}" /tmp || rm -f "${PKGPATH}"
                    mv -v "${PKGPATH}" "Artifacts/${PKGFILE/Scratch Link-notarizeMe/Scratch Link}"
                  done
      - store_artifacts:
          path: Artifacts/
commands:
  do_mac_build:
    parameters:
      configuration:
        default: "Release"
        type: string
      artifact_tag:
        default: ""
        type: string
    steps:
      - run:
          name: "Build for Mac: <<parameters.configuration>>"
          command: |
            msbuild -m -t:Restore -p:Configuration=<<parameters.configuration>> scratch-link.sln
            "/Applications/Visual Studio.app/Contents/MacOS/vstool" build -t:Build -c:<<parameters.configuration>> "${PWD}/scratch-link.sln"
            # "for" is just a convenient way to resolve the glob to a variable so we can DRY the filename for "if" and "cp"
            for PKGPATH in scratch-link-mac/bin/<<parameters.configuration>>/"Scratch Link"*.pkg; do
              if [ -r "$PKGPATH" ]; then
                PKGFILE="${PKGPATH##*/}"
                if [ -n "<<parameters.artifact_tag>>" ]; then
                  PKGFILE="${PKGFILE/Scratch Link/Scratch Link-<<parameters.artifact_tag>>}"
                fi
                mkdir -p Artifacts
                cp -v "$PKGPATH" "Artifacts/${PKGFILE}"
              fi
            done
